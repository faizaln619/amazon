package com.javap.employee.dto;

import java.io.Serializable;
import java.util.Date;

public class LeaveDto implements Serializable {
	
	private long employee_Id;
	
	private Date employee_Start_Date;
	
	private Date employee_End_Date;

	public long getEmployee_Id() {
		return employee_Id;
	}

	public void setEmployee_Id(long employee_Id) {
		this.employee_Id = employee_Id;
	}

	public Date getEmployee_Start_Date() {
		return employee_Start_Date;
	}

	public void setEmployee_Start_Date(Date employee_Start_Date) {
		this.employee_Start_Date = employee_Start_Date;
	}

	public Date getEmployee_End_Date() {
		return employee_End_Date;
	}

	public void setEmployee_End_Date(Date employee_End_Date) {
		this.employee_End_Date = employee_End_Date;
	}

	@Override
	public String toString() {
		return "LeaveDto [employee_Id=" + employee_Id + ", employee_Start_Date=" + employee_Start_Date
				+ ", employee_End_Date=" + employee_End_Date + "]";
	}
	
	
	
}
