package com.javap.employee.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.javap.employee.entity.LeaveEntity;

public interface LeaveRepository extends JpaRepository<LeaveEntity, Long> {

}
